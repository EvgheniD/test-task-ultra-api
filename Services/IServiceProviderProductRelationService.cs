namespace test_task_ultra_api.Services
{
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public interface IServiceProviderProductRelationService
    {
            public Task<PaginatedResponse<ProductModel>> GetServiceProviderProducts(QueryModel queryModel, string serviceProviderId);
            public Task<Boolean> AddProductToServiceProvider (string serviceProviderId, string productId);
            public Task<Boolean> RemoveProductFromServiceProvider (string serviceProviderId, string productId);
    }
}