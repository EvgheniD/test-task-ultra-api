namespace test_task_ultra_api.Services
{
    using Microsoft.EntityFrameworkCore;
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public class ServiceProviderProductRelationService : IServiceProviderProductRelationService
    {
        private readonly DataContext dataContext;
        public ServiceProviderProductRelationService(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<PaginatedResponse<ProductModel>> GetServiceProviderProducts(QueryModel query, string serviceProviderId)
        {
            //eager loading the product entities
            return await Task.Run(() =>{
                var sppr = dataContext.ServiceProviderProductRelations
                    .Include(sppr => sppr.Product);
                var selectedProducts = sppr.Where(sppr => sppr.Product != null && sppr.UuidSp.Equals(serviceProviderId))
                                .Select(sppr => sppr.Product).Cast<ProductModel>();
                
                var queredProducts = QueryModel.ApplyQueryToEntity(selectedProducts, query);
                                
                return new PaginatedResponse<ProductModel>(
                    query.PageNumber,
                    query.PageSize,
                    queredProducts.Count(),
                    queredProducts.ToList()
                );
           });
        }

        public async Task<Boolean> AddProductToServiceProvider(string serviceProviderId, string productId)
        {
            await dataContext.ServiceProviderProductRelations
            .AddAsync(new Models.ServiceProviderProductRelation { UuidSp = serviceProviderId, UuidProduct = productId });
            await dataContext.SaveChangesAsync();

            return true;
        }

        public async Task<Boolean> RemoveProductFromServiceProvider(string serviceProviderId, string productId) 
        { 
            var relationToRemove = await dataContext.ServiceProviderProductRelations
            .FirstOrDefaultAsync(sppr => sppr.UuidSp == serviceProviderId && sppr.UuidProduct == productId);
            if (relationToRemove != null)
            {
                dataContext.ServiceProviderProductRelations.Remove(relationToRemove);
                await dataContext.SaveChangesAsync();

                return true;
            }

            return false; 
        }

    } 
}