﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace test_task_ultra_api.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "product",
                columns: table => new
                {
                    uuid = table.Column<string>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "TEXT", nullable: false),
                    weight = table.Column<double>(type: "REAL", nullable: false),
                    volume = table.Column<int>(type: "INTEGER", nullable: false),
                    cost = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "service_provider",
                columns: table => new
                {
                    uuid = table.Column<string>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "TEXT", nullable: false),
                    description = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_service_provider", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "delivery",
                columns: table => new
                {
                    uuid = table.Column<string>(type: "TEXT", nullable: false),
                    name = table.Column<string>(type: "TEXT", nullable: false),
                    uuid_product = table.Column<string>(type: "TEXT", nullable: false),
                    uuid_sp = table.Column<string>(type: "TEXT", nullable: false),
                    create_datetime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    delivered = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_delivery", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_delivery_product_uuid_product",
                        column: x => x.uuid_product,
                        principalTable: "product",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_delivery_service_provider_uuid_sp",
                        column: x => x.uuid_sp,
                        principalTable: "service_provider",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "rel_sp_product",
                columns: table => new
                {
                    uuid_sp = table.Column<string>(type: "TEXT", nullable: false),
                    uuid_product = table.Column<string>(type: "TEXT", nullable: false),
                    _ServiceProviderServiceProviderId = table.Column<string>(type: "TEXT", nullable: false),
                    ProductId = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rel_sp_product", x => new { x.uuid_product, x.uuid_sp });
                    table.ForeignKey(
                        name: "FK_rel_sp_product_product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "product",
                        principalColumn: "uuid");
                    table.ForeignKey(
                        name: "FK_rel_sp_product_service_provider__ServiceProviderServiceProviderId",
                        column: x => x._ServiceProviderServiceProviderId,
                        principalTable: "service_provider",
                        principalColumn: "uuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_product",
                table: "delivery",
                column: "uuid_product",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_sp",
                table: "delivery",
                column: "uuid_sp",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_rel_sp_product__ServiceProviderServiceProviderId",
                table: "rel_sp_product",
                column: "_ServiceProviderServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_rel_sp_product_ProductId",
                table: "rel_sp_product",
                column: "ProductId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "delivery");

            migrationBuilder.DropTable(
                name: "rel_sp_product");

            migrationBuilder.DropTable(
                name: "product");

            migrationBuilder.DropTable(
                name: "service_provider");
        }
    }
}
