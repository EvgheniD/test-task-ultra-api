﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace test_task_ultra_api.Migrations
{
    /// <inheritdoc />
    public partial class fixForUniqueConstraits : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_delivery_uuid_product",
                table: "delivery");

            migrationBuilder.DropIndex(
                name: "IX_delivery_uuid_sp",
                table: "delivery");

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_product",
                table: "delivery",
                column: "uuid_product");

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_sp",
                table: "delivery",
                column: "uuid_sp");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_delivery_uuid_product",
                table: "delivery");

            migrationBuilder.DropIndex(
                name: "IX_delivery_uuid_sp",
                table: "delivery");

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_product",
                table: "delivery",
                column: "uuid_product",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_delivery_uuid_sp",
                table: "delivery",
                column: "uuid_sp",
                unique: true);
        }
    }
}
