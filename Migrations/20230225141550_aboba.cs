﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace test_task_ultra_api.Migrations
{
    /// <inheritdoc />
    public partial class aboba : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_product_uuid_sp",
                table: "rel_sp_product");

            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_product",
                table: "rel_sp_product");

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_product_uuid_product",
                table: "rel_sp_product",
                column: "uuid_product",
                principalTable: "product",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_sp",
                table: "rel_sp_product",
                column: "uuid_sp",
                principalTable: "service_provider",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_product_uuid_product",
                table: "rel_sp_product");

            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_sp",
                table: "rel_sp_product");

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_product_uuid_sp",
                table: "rel_sp_product",
                column: "uuid_sp",
                principalTable: "product",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_product",
                table: "rel_sp_product",
                column: "uuid_product",
                principalTable: "service_provider",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
