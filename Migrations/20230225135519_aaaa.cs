﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace test_task_ultra_api.Migrations
{
    /// <inheritdoc />
    public partial class aaaa : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_product_ProductId",
                table: "rel_sp_product");

            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_service_provider__ServiceProviderServiceProviderId",
                table: "rel_sp_product");

            migrationBuilder.DropIndex(
                name: "IX_rel_sp_product__ServiceProviderServiceProviderId",
                table: "rel_sp_product");

            migrationBuilder.DropIndex(
                name: "IX_rel_sp_product_ProductId",
                table: "rel_sp_product");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "rel_sp_product");

            migrationBuilder.DropColumn(
                name: "_ServiceProviderServiceProviderId",
                table: "rel_sp_product");

            migrationBuilder.CreateIndex(
                name: "IX_rel_sp_product_uuid_sp",
                table: "rel_sp_product",
                column: "uuid_sp");

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_product_uuid_sp",
                table: "rel_sp_product",
                column: "uuid_sp",
                principalTable: "product",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_product",
                table: "rel_sp_product",
                column: "uuid_product",
                principalTable: "service_provider",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_product_uuid_sp",
                table: "rel_sp_product");

            migrationBuilder.DropForeignKey(
                name: "FK_rel_sp_product_service_provider_uuid_product",
                table: "rel_sp_product");

            migrationBuilder.DropIndex(
                name: "IX_rel_sp_product_uuid_sp",
                table: "rel_sp_product");

            migrationBuilder.AddColumn<string>(
                name: "ProductId",
                table: "rel_sp_product",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "_ServiceProviderServiceProviderId",
                table: "rel_sp_product",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_rel_sp_product__ServiceProviderServiceProviderId",
                table: "rel_sp_product",
                column: "_ServiceProviderServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_rel_sp_product_ProductId",
                table: "rel_sp_product",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_product_ProductId",
                table: "rel_sp_product",
                column: "ProductId",
                principalTable: "product",
                principalColumn: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_rel_sp_product_service_provider__ServiceProviderServiceProviderId",
                table: "rel_sp_product",
                column: "_ServiceProviderServiceProviderId",
                principalTable: "service_provider",
                principalColumn: "uuid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
