using Microsoft.EntityFrameworkCore;
using test_task_ultra_api.Models;

public class DataContext : DbContext 
{
    public DbSet<DeliveryModel> Deliveries {get;set;}
    public DbSet<ProductModel> Products {get;set;}
    public DbSet<ServiceProviderModel> ServiceProviders {get;set;}
    public DbSet<ServiceProviderProductRelation> ServiceProviderProductRelations {get;set;}

    protected override void OnConfiguring(DbContextOptionsBuilder options)
                => options.UseSqlite($"Data Source = ServiceProduct.db");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //Service provider relation related
        modelBuilder.Entity<ServiceProviderProductRelation>()
            .HasKey(spr => new {spr.UuidProduct, spr.UuidSp});
        modelBuilder.Entity<ServiceProviderProductRelation>()
            .HasOne(spr => spr.Product)
            .WithMany(p => p.serviceProviderProductRelations)
            .HasForeignKey(spr => spr.UuidProduct);
        modelBuilder.Entity<ServiceProviderProductRelation>()
            .HasOne(spr => spr._ServiceProvider)
            .WithMany(sp => sp.serviceProviderProductRelations)
            .HasForeignKey(spr => spr.UuidSp);

        //Delivery related
        modelBuilder.Entity<DeliveryModel>()
            .HasOne(d => d.Product)
            .WithOne()
            .HasForeignKey<DeliveryModel>(d => d.UuidProduct);
        
        modelBuilder.Entity<DeliveryModel>()
            .HasIndex(d => d.UuidProduct)
            .IsUnique(false);

        modelBuilder.Entity<DeliveryModel>()
            .HasOne(d => d.Sp)
            .WithOne()
            .HasForeignKey<DeliveryModel>(d => d.UuidSp);
        
        modelBuilder.Entity<DeliveryModel>()
            .HasIndex(d => d.UuidSp)
            .IsUnique(false);
    }
}