# Ultra-API

Ultra-API is a .NET Core API project that provides endpoints for various functionalities. The project is built using .NET Core 7 and Entity Framework Core.

## Endpoints

All endpoints can be observed via Swagger UI, which can be accessed by navigating to the root of the application on port 7217 (i.e., `https://localhost:7217`).

## Authentication

Authentication is enabled using basic JWT authentication. To obtain the JWT token, use the following credentials:

- Username: `username`
- Password: `password`

Note that the secret key used for JWT authentication is intended for development only.

## Testing

Swagger has been added to enable easy testing of the API endpoints. The Swagger UI can be used to test the endpoints and view the API documentation.(i.e., `https://localhost:7217/swagger/index.html`)

## Technologies Used

- .NET Core 7
- Entity Framework Core
- Sql lite
