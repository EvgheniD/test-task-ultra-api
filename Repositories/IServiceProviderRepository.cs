namespace test_task_ultra_api.Repositories
{
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public interface IServiceProviderRepository
    {
            public Task<PaginatedResponse<ServiceProviderModel>> GetServiceProviders(QueryModel queryModel);
            public Task<ServiceProviderModel> GetServiceProviderById (string serviceProviderId);
            public Task<ServiceProviderModel> CreateServiceProvider(ServiceProviderModel serviceProviderModel);
            public Task<ServiceProviderModel> UpdateServiceProvider(ServiceProviderModel serviceProviderModel);
            public Task<bool> DeleteServiceProvider(string serviceProviderId); 
    }
}