namespace test_task_ultra_api.Repositories
{
    using test_task_ultra_api.UtilityModels;
    using test_task_ultra_api.Models;

    public interface IDeliveryRepository
    {
        public Task<PaginatedResponse<DeliveryModel>> GetDeliveries(DeliveryQueryModel queryModel);
        public Task<DeliveryModel> GetDeliveryById (string deliveryId);
        public Task<DeliveryModel> CreateDelivery(DeliveryModel deliveryModel);
        public Task<DeliveryModel> UpdateDelivery(DeliveryModel deliveryModel);
        public Task<bool> DeleteDelivery(string deliveryId); 
    }
}