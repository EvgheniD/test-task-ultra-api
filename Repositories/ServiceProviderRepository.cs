
namespace test_task_ultra_api.Repositories
{
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public class ServiceProviderRepository : IServiceProviderRepository
    {
        private DataContext _context;

        public ServiceProviderRepository(DataContext dataContext) 
        {
            this._context = dataContext;
        }

        public async Task<ServiceProviderModel> GetServiceProviderById(string serviceProviderId)
        {
            return await Task.Run(() => 
            {
                var foundServiceProvider = _context.ServiceProviders.FirstOrDefault(p => p.ServiceProviderId.Equals(serviceProviderId));
                if (foundServiceProvider!= null)
                    return foundServiceProvider;
                else
                    throw new Exception("Service Provider not found");
            });
        }

        public async Task<PaginatedResponse<ServiceProviderModel>> GetServiceProviders(QueryModel query) 
        { 
           return await Task.Run(() => 
            { 
                var quariedServiceProviders = QueryModel.ApplyQueryToEntity(_context.ServiceProviders.AsQueryable(),query); 
                return new PaginatedResponse<ServiceProviderModel>( 
                    query.PageNumber, 
                    query.PageSize, 
                    quariedServiceProviders.Count(), 
                    quariedServiceProviders.ToList() 
                );  
            });  

        }  

         public async Task<ServiceProviderModel> CreateServiceProvider(ServiceProviderModel serviceProvider)  
         {  
              serviceProvider.ServiceProviderId = Guid.NewGuid().ToString("N");
              var newServiceProvider = _context.ServiceProviders.Add(serviceProvider);  
              await _context.SaveChangesAsync();  
              return newServiceProvider.Entity;  
         }  

         public async Task<ServiceProviderModel> UpdateServiceProvider(ServiceProviderModel serviceProviderModel)   
         {    
             var updateServiceProvider = _context.ServiceProviders.Update(serviceProviderModel);    
             await _context.SaveChangesAsync();
             return updateServiceProvider.Entity;
         }

         public async Task<bool> DeleteServiceProvider(String serviceProviderId)
         {
            var serviceProviderToDelete = await _context.ServiceProviders.FindAsync(serviceProviderId);
            if (serviceProviderToDelete == null) return false;
            _context.ServiceProviders.Remove(serviceProviderToDelete);
            await _context.SaveChangesAsync();
            return true;
         }
    }
}