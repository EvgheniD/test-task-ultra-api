namespace test_task_ultra_api.Repositories
{
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public interface IProductRepository
    {
        public Task<PaginatedResponse<ProductModel>> GetProducts(QueryModel queryModel);
        public Task<ProductModel> GetProductById (string productId);
        public Task<ProductModel> CreateProduct(ProductModel productModel);
        public Task<ProductModel> UpdateProduct(ProductModel productModel);
        public Task<bool> DeleteProduct(string productId);
    }
}