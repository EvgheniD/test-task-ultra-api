namespace test_task_ultra_api.Repositories
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public class DeliveryRepository : IDeliveryRepository
    {
        private DataContext _context;

        public DeliveryRepository(DataContext dataContext) 
        {
            this._context = dataContext;
        }

        public async Task<DeliveryModel> GetDeliveryById(string deliveryId)
        {
            var foundDelivery = await _context.Deliveries.FindAsync(deliveryId);
            if (foundDelivery != null)
                return foundDelivery;
            else
                throw new Exception("Delivery not found");
        }

        public async Task<PaginatedResponse<DeliveryModel>> GetDeliveries(DeliveryQueryModel query) 
        { 
            return await Task.Run(() =>{
                //Here to do eager loading
                var deliveries = _context.Deliveries.
                    Include(d => d.Sp);
                var quariedDeliveries = DeliveryQueryModel.ApplyDeliveryQueryToEntity(_context.Deliveries.AsQueryable(), query);
                return new PaginatedResponse<DeliveryModel>(
                    query.PageNumber, 
                    query.PageSize, 
                    quariedDeliveries.Count(), 
                    quariedDeliveries.ToList()
                );
            });
        }

        public async Task<DeliveryModel> CreateDelivery(DeliveryModel delivery)  
        {  
            delivery.Uuid = Guid.NewGuid().ToString("N");
            var newDelivery = _context.Deliveries.Add(delivery);  
            await _context.SaveChangesAsync();  
            return newDelivery.Entity;  
        }  

        public async Task<DeliveryModel> UpdateDelivery(DeliveryModel delivery)   
        {    
            var updateDelivery = _context.Deliveries.Update(delivery);    
            await _context.SaveChangesAsync();
            return updateDelivery.Entity;
        }

        public async Task<bool> DeleteDelivery(string deliveryId)
        {
            var deliveryToDelete = await _context.Deliveries.FindAsync(deliveryId);
            if (deliveryToDelete == null) return false;
            _context.Deliveries.Remove(deliveryToDelete);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
