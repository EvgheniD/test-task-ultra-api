namespace test_task_ultra_api.Repositories
{
    using test_task_ultra_api.Models;
    using test_task_ultra_api.UtilityModels;

    public class ProductRepository : IProductRepository
    {
        private DataContext _context;

        public ProductRepository(DataContext dataContext) 
        {
            this._context = dataContext;
        }

        public async Task<ProductModel> GetProductById(string productId)
        {
            return await Task.Run(() => 
            {
                var foundProduct = _context.Products.FirstOrDefault(p => p.ProductId.Equals(productId));
                if (foundProduct!= null)
                    return foundProduct;
                else
                    throw new Exception("Product not found");
            });
        }

        public async Task<PaginatedResponse<ProductModel>> GetProducts(QueryModel query)
        {
           return await Task.Run(() => 
            {
                var quariedProducts = QueryModel.ApplyQueryToEntity(_context.Products.AsQueryable(),query);
                return new PaginatedResponse<ProductModel>(
                    query.PageNumber,
                    query.PageSize,
                    quariedProducts.Count(),
                    quariedProducts.ToList()
                ); 
            });
        }
        
        public async Task<ProductModel> CreateProduct(ProductModel productModel)
        {
              productModel.ProductId = Guid.NewGuid().ToString("N");
              var newProduct = _context.Products.Add(productModel);
              await _context.SaveChangesAsync();
              return newProduct.Entity;
        }
        
        public async Task<ProductModel> UpdateProduct(ProductModel productModel) 
        { 
             var updatedProduct = _context.Products.Update(productModel); 
             await _context.SaveChangesAsync(); 
             return updatedProduct.Entity; 
        } 

        public async Task<bool> DeleteProduct(string productId) 
        { 
            var productToDelete = await _context.Products.FindAsync(productId);
            if (productToDelete == null) return false;
            _context.Products.Remove(productToDelete); 
            await _context.SaveChangesAsync();
            return true; 
        }
    }
}