namespace test_task_ultra_api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using test_task_ultra_api.Models;
    using test_task_ultra_api.Repositories;
    using test_task_ultra_api.UtilityModels;
    using test_task_ultra_api.Services;
    using Microsoft.AspNetCore.Authorization;

    [ApiController]
    [Route("api/v1/sp")]
    public class ServiceProviderController : ControllerBase
    {
        private readonly IServiceProviderRepository _serviceProviderRepository;
        private readonly IServiceProviderProductRelationService _serviceProviderProductRelationService;

        public ServiceProviderController(IServiceProviderRepository serviceProviderRepository, IServiceProviderProductRelationService serviceProviderProductRelationService)
        {
            _serviceProviderRepository = serviceProviderRepository;
            _serviceProviderProductRelationService = serviceProviderProductRelationService;
        }

        [Authorize]
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<PaginatedResponse<ServiceProviderModel>>> GetServiceProviders([FromQuery] QueryModel queryModel)
        {
            try
            {
                return Ok(await _serviceProviderRepository.GetServiceProviders(queryModel));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("{uuid}")]
        public async Task<ActionResult<ServiceProviderModel>> GetServiceProviderById(string uuid)
        {
            try
            {
                return Ok(await _serviceProviderRepository.GetServiceProviderById(uuid));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<ServiceProviderModel>> CreateServiceProvider([FromBody] ServiceProviderModel serviceProvider)
        {
            try
            {
                return Ok(await _serviceProviderRepository.CreateServiceProvider(serviceProvider));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPatch]
        [Route("")]
        public async Task<ActionResult<ServiceProviderModel>> UpdateServiceProvider([FromBody] ServiceProviderModel serviceProvider)
        {
            try
            {
                return Ok(await _serviceProviderRepository.UpdateServiceProvider(serviceProvider));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpDelete]
        [Route("{uuid}")]
        public async Task<ActionResult<bool>> DeleteServiceProvider(string uuid)
        {
            try
            {
                return Ok(await _serviceProviderRepository.DeleteServiceProvider(uuid));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("{uuid}/product")]
        public async Task<ActionResult<PaginatedResponse<ProductModel?>>> GetServiceProviderProducts([FromQuery] QueryModel query, string uuid) 
        {  
            try 
            {
                var response = await _serviceProviderProductRelationService.GetServiceProviderProducts(query, uuid); 
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
             
        }

        [Authorize]
        [HttpPost]
        [Route("{uuid}/product/{puuid}")]
        public async Task<ActionResult<bool>> AddProductToServiceProvider(string uuid, string puuid) 
        {  
            try 
            {
                var response = await _serviceProviderProductRelationService.AddProductToServiceProvider(uuid, puuid); 
                return Ok(response);
            }
                catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize] 
        [HttpDelete]  
        [Route("{uuid}/product/{puuid}")]
        public async Task<ActionResult<bool>> RemoveProductFromServiceProvide(string uuid, string puuid)  
        {  
            try 
            {
                var response = await _serviceProviderProductRelationService.RemoveProductFromServiceProvider(uuid, puuid); 
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}