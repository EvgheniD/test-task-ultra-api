namespace test_task_ultra_api.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using test_task_ultra_api.Models;
    using test_task_ultra_api.Repositories;
    using test_task_ultra_api.UtilityModels;

    [ApiController]
    [Route("api/v1/deliveries")]
    public class DeliveryController : ControllerBase
    {
        private readonly IDeliveryRepository _deliveryRepository;

        public DeliveryController(IDeliveryRepository deliveryRepository)
        {
            _deliveryRepository = deliveryRepository;
        }

        [Authorize]
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<PaginatedResponse<DeliveryModel>>> GetDeliveries([FromQuery] DeliveryQueryModel deliveryQuery)
        {
            try
            {
                return Ok(await _deliveryRepository.GetDeliveries(deliveryQuery));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("{uuid}")]
        public async Task<ActionResult<DeliveryModel>> GetDeliveryById(String uuid)
        {
            try
            {
                return Ok(await _deliveryRepository.GetDeliveryById(uuid));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<DeliveryModel>> CreateDelivery([FromBody] DeliveryModel delivery)
        {
            try
            {
                return Ok(await _deliveryRepository.CreateDelivery(delivery));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException?.Message);
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPatch]
        [Route("")]
        public async Task<ActionResult<DeliveryModel>> UpdateDelivery([FromBody] DeliveryModel delivery)
        {
            try
            {
                return Ok(await _deliveryRepository.UpdateDelivery(delivery));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpDelete]
        [Route("{uuid}")]
        public async Task<ActionResult<bool>> DeleteDelivery(string uuid)
        {
            try
            {
                return Ok(await _deliveryRepository.DeleteDelivery(uuid));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
