namespace test_task_ultra_api.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using test_task_ultra_api.Models;
    using test_task_ultra_api.Repositories;
    using test_task_ultra_api.UtilityModels;

    [ApiController]
    [Route("api/v1/product")]
    public class ProductController : ControllerBase
    {
        private IProductRepository productRepository;

        public ProductController(IProductRepository productRepository) {
            this.productRepository = productRepository;
        }

        [HttpGet]
        [Authorize]
        [Route("")]
        public async Task<ActionResult<PaginatedResponse<ProductModel>>> GetProduct([FromQuery] QueryModel queryModel)
        {
            try {
                return Ok(await productRepository.GetProducts(queryModel));
            } catch(Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Authorize]
        [Route("{uuid}")]
        public async Task<ActionResult<ProductModel>> GetProductById(string uuid)
        {
            try {
                return Ok(await productRepository.GetProductById(uuid));
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Authorize]
        [Route("")]
        public async Task<ActionResult<ProductModel>> CreateProduct([FromBody] ProductModel product)
        {
            try {
                return Ok(await productRepository.CreateProduct(product));
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpPatch]
        [Authorize]
        [Route("")]
        public async Task<ActionResult<ProductModel>> UpdateProduct([FromBody] ProductModel product)
        {
            try {
                return Ok(await productRepository.UpdateProduct(product));
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        [Authorize]
        [Route("{uuid}")]
        public async Task<ActionResult<bool>> DeleteProduct(string uuid) 
        {
            try {
                return Ok(await productRepository.DeleteProduct(uuid));
            } catch (Exception e) {
                return BadRequest(e.Message);
            }
        }
    }
}