namespace test_task_ultra_api.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("service_provider")]
    public class ServiceProviderModel
    {
        [Key]
        [Column("uuid")]
        [JsonPropertyName("uuid")]
        public string ServiceProviderId {get;set;}

        [Required]
        [Column("name")]
        [JsonPropertyName("name")]
        public string Name {get;set;}

        [Required]
        [Column("description")]
        [JsonPropertyName("description")]
        public string Description {get;set;}

        [JsonIgnore]
        public ICollection<ServiceProviderProductRelation>? serviceProviderProductRelations {get;set;} 

    }
}