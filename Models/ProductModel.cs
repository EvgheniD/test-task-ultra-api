namespace test_task_ultra_api.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("product")]
    public class ProductModel
    {
        [Key]
        [Column("uuid")]
        [JsonPropertyName("uuid")]
        public string ProductId {get;set;}

        [Required]
        [Column("name")]
        [JsonPropertyName("name")]
        public string Name {get;set;}
    
        [Required]
        [Column("weight")]
        [JsonPropertyName("weight")]
        public double Weight {get;set;}

        [Required]
        [Column("volume")]
        [JsonPropertyName("volume")]
        public int Volume {get;set;}

        [Required]
        [Column("cost")]
        [JsonPropertyName("cost")]
        public int Cost {get;set;}

        [JsonIgnore]
        public ICollection<ServiceProviderProductRelation>? serviceProviderProductRelations {get;set;} 
    }
}