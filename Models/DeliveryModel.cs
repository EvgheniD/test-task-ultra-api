namespace test_task_ultra_api.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("delivery")]
    public class DeliveryModel
    {
        [Key]
        [Column("uuid")]
        public string Uuid { get; set; }

        [Required]
        [Column("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [Required]
        [Column("uuid_product")]
        [JsonPropertyName("uuid_product")]
        public string UuidProduct { get; set; }

        [Required]
        [Column("uuid_sp")]
        [JsonPropertyName("uuid_sp")]
        public string UuidSp { get; set; }

        [Column("create_datetime")]
        [JsonPropertyName("create_datetime")]
        public DateTime CreateDateTime { get; set; }

        [Required]
        [Column("delivered")]
        [JsonPropertyName("delivered")]
        public bool Delivered { get; set; }

        [JsonIgnore]
        public ProductModel? Product { get; set; }

        [JsonIgnore]
        public ServiceProviderModel? Sp { get; set; }
    }
}