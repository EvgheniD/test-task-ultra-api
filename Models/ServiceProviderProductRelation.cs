namespace test_task_ultra_api.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("rel_sp_product")]
    public class ServiceProviderProductRelation
    {
        [Column("uuid_sp")]
        [JsonPropertyName("uuid_sp")]
        public string UuidSp {get;set;}

        [Column("uuid_product")]
        [JsonPropertyName("uuid_product")]
        public string UuidProduct{get; set;}

        [JsonIgnore]
        public ProductModel? Product {get;set;}

        [JsonIgnore]
        public ServiceProviderModel? _ServiceProvider {get;set;}
    }
}