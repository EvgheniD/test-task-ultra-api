namespace test_task_ultra_api.UtilityModels
{
    using test_task_ultra_api.Models;

    public class QueryModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? SearchTerm { get; set; }
        public bool IsAscending { get; set; }

        public static IEnumerable<ProductModel> ApplyQueryToEntity (IQueryable<ProductModel> products, QueryModel query) 
        {
            if (!string.IsNullOrEmpty(query.SearchTerm))
            {
                products = products.Where(p => p.Name.Contains(query.SearchTerm));
            }
            //Order by name, IsAscending
            products = query.IsAscending ? 
                                    products.OrderBy(d => d.Name) : 
                                    products.OrderByDescending(d => d.Name);
            //Paginate
            products = products.Skip((query.PageNumber - 1) * query.PageSize).Take(query.PageSize);
            return products.AsEnumerable();
        }

        

        public static IEnumerable<ServiceProviderModel> ApplyQueryToEntity (IQueryable<ServiceProviderModel> sps, QueryModel query) 
        {
            if (!string.IsNullOrEmpty(query.SearchTerm))
            {
                sps = sps.Where(sp => sp.Name.Contains(query.SearchTerm));
            }
            //Order by name, IsAscending
            sps = query.IsAscending ? 
                                    sps.OrderBy(d => d.Name) : 
                                    sps.OrderByDescending(d => d.Name);
            //Paginate
            sps = sps.Skip((query.PageNumber - 1) * query.PageSize).Take(query.PageSize);
            return sps.AsEnumerable();
        }
    }
}