using test_task_ultra_api.Models;

namespace test_task_ultra_api.UtilityModels
{
    public class DeliveryQueryModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string? ServiceProviderName { get; set; }
        public bool? DeliveryStatus {get;set;}
        public bool IsAscending { get; set; }
    
        public static IEnumerable<DeliveryModel> ApplyDeliveryQueryToEntity (IQueryable<DeliveryModel> deliveries, DeliveryQueryModel query) 
        {
            // for now every delivery query will also go with eager loading of service provider
            if (!string.IsNullOrEmpty(query.ServiceProviderName))
            {
                deliveries = deliveries.Where(d => d.Sp != null && d.Sp.Name.Equals(query.ServiceProviderName));
            }

            if (query.DeliveryStatus != null)
            {
                deliveries = deliveries.Where(d => d.Delivered == query.DeliveryStatus);
            }
            deliveries = query.IsAscending ? 
                                        deliveries.OrderBy(d => d.CreateDateTime) : 
                                        deliveries.OrderByDescending(d => d.CreateDateTime);
                // Paginate
            deliveries = deliveries.Skip((query.PageNumber - 1) * query.PageSize).Take(query.PageSize);
            return deliveries.AsEnumerable();
        }
    }
}